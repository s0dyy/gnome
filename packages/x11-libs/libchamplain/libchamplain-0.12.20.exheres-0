# Copyright 2009, 2011, 2013 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ with_opt=true ]
require meson

SUMMARY="Map display library based on Clutter"
DESCRIPTION="
libchamplain supports numerous free map sources such as OpenStreetMap, OpenCycleMap,
Öpnvkarte, OpenAerialMap and Maps for free.
libchamplain is named after Samuel de Champlain, famous French navigator, explorer and
cartographer. He is the 'father of New-France' and founder of Québec City, which is
the 400th anniversary in 2008, the year that this library was first created.
"
HOMEPAGE="http://projects.gnome.org/${PN}"

LICENCES="LGPL-2.1"
SLOT="0.12"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="gobject-introspection gtk-doc

    vapi [[
        description = [ Build Vala bindings ]
        requires = [ gobject-introspection ]
    ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.3] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
    build+run:
        dev-db/sqlite:3[>=3.0]
        dev-libs/glib:2[>=2.38]
        gnome-desktop/libsoup:2.4[>=2.42]
        x11-libs/cairo[>=1.4]
        x11-libs/clutter:1[>=1.24.0][gobject-introspection?]
        x11-libs/clutter-gtk:1.0
        x11-libs/gtk+:3[>=3.0][gobject-introspection?]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddemos=false
    -Dwidgetry=true

    # missing dependency libmemphis
    -Dmemphis=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'vapi vala'
)

