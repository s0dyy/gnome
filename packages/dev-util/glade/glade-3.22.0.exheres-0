# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require python [ blacklist=none with_opt='true' multibuild=false ]
require freedesktop-desktop gtk-icon-cache

SUMMARY="GTK+ UI Designer"
HOMEPAGE="https://glade.gnome.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc python
    webkit [[ description = [ build webkit2gtk catalog ] ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt [[ note = [ for manpages ] ]]
        dev-util/intltool[>=0.41.0]
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        dev-libs/glib:2[>=2.53.2]
        dev-libs/libxml2:2.0[>=2.4.0]
        x11-libs/gtk+:3[>=3.20.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.32.0] )
        python? ( gnome-bindings/pygobject:3[>=3.8.0][python_abis:*(-)?] )
        webkit? ( net-libs/webkit:4.0[>=2.12] )
"

# Require X11
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-gladeui'
    '--enable-man-pages'
    '--disable-debug'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'python'
    'webkit webkit2gtk'
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

